package com.example.ubfreeroom;

import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class HTMLRequester {

    private final URL url;
    private final Utilities utilities;
    private final Map<String, String> forcedContent;

    public HTMLRequester(Map<String, InputStream> streams) throws IOException {
        forcedContent = new HashMap<>();
        utilities = new Utilities(streams);
        url = new URL("https://celcat.u-bordeaux.fr/Calendar/Home/GetCalendarData");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
        Date date = new Date();
        forcedContent.put("start", dateFormat.format(date));
        forcedContent.put("end", dateFormat.format(date));
        forcedContent.put("resType", "102");
    }

    public Utilities getUtilities() {
        return utilities;
    }

    private Date tomorrow(Date date) {
        Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    private void idsPut(Map<String, String> map, String id) {
        map.put("federationIds[]", id);
    }

    public ArrayList<Object> requestBat(String bat) {
        ArrayList<Object> ret = new ArrayList<>();

        StringBuilder sbParams = new StringBuilder();
        sbParams.append("calView").append("=").append("agendaDay");

        for (String k : forcedContent.keySet()) {
            sbParams.append("&").append(Uri.encode(k)).append("=").append(Uri.encode(forcedContent.get(k)));
        }

        switch (bat) {
            case "a9" -> {
                for (String s : utilities.getA9map().keySet()) {
                    sbParams.append("&").append("federationIds%5B%5D").append("=").append(Uri.encode(s));
                }
            }
            case "a22" -> {
                for (String s : utilities.getA22map().keySet()) {
                    sbParams.append("&").append("federationIds%5B%5D").append("=").append(Uri.encode(s));
                }
            }
            case "a28" -> {
                for (String s : utilities.getA28map().keySet()) {
                    sbParams.append("&").append("federationIds%5B%5D").append("=").append(Uri.encode(s));
                }
            }
            case "a29" -> {
                for (String s : utilities.getA29map().keySet()) {
                    sbParams.append("&").append("federationIds%5B%5D").append("=").append(Uri.encode(s));
                }
            }
        }

        HashMap<String, ArrayList<HashMap<String, Object>>> map;

        try {
            String res = connection(sbParams);
            System.out.println(res);

            map = new ObjectMapper().readValue(res, HashMap.class);

        } catch (IOException exception) {
            Log.e("query", "requestBat: ", exception);
            ret.add(0, false);
            return ret;
        }

        ret.add(0, true);
        ret.add(1, map);
        return ret;
    }

    private String connection(StringBuilder sbParams) throws IOException {
        HttpURLConnection client = (HttpURLConnection) url.openConnection();
        client.setDoOutput(true);
        client.setRequestMethod("POST");
        client.setReadTimeout(10000);
        client.setConnectTimeout(15000);
        client.connect();

        String paramsString = sbParams.toString();

        DataOutputStream wr = new DataOutputStream(client.getOutputStream());
        wr.writeBytes(paramsString);
        wr.flush();
        wr.close();

        InputStream in = new BufferedInputStream(client.getInputStream());

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        result.append("{\"result\":");
        String line;
        while ((line = reader.readLine()) != null) {

//            String unescaped = String.valueOf(Html.fromHtml(line, Html.FROM_HTML_MODE_LEGACY));
            result.append(line);
        }
        result.append("}");

        client.disconnect();
        return result.toString();
    }

}
