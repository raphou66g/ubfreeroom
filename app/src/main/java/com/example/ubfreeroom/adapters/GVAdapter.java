package com.example.ubfreeroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ubfreeroom.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class GVAdapter extends ArrayAdapter {

    private final HashMap<String, ArrayList<ArrayList<String>>> rooms;
    private final ArrayList<String> names;

    public GVAdapter(Context context, int textViewResourceId, HashMap<String, ArrayList<ArrayList<String>>> objects, ArrayList<String> keys) {
        super(context, textViewResourceId, keys);
        this.rooms = objects;
        this.names = keys;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.grid_view_items, null);
        TextView tv = v.findViewById(R.id.grid_tv);
        String name = names.get(position);
        String[] ls = name.split(" ");
        tv.setText(ls[ls.length - 1]);
        if (rooms.get(name) != null) {
            ArrayList<ArrayList<String>> stamps = rooms.get(name);
            if (stamps.isEmpty()) {
                tv.setTextColor(getContext().getColor(R.color.free));
            } else if (stamps.get(0).get(0).matches("allDay")) {
                tv.setTextColor(getContext().getColor(R.color.occupied));
            } else {
                Date date = new Date();
                Date future = future(date);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
                for (ArrayList<String> dates : stamps) {
                    try {
                        Date date1 = dateFormat.parse(dates.get(0).replace("T", " "));
                        if (future.before(date1)){
                            tv.setTextColor(getContext().getColor(R.color.free));
                            break;
                        } else {
                            Date date2 = dateFormat.parse(dates.get(1).replace("T", " "));
                            if (date.before(date2)){
                                tv.setTextColor(getContext().getColor(R.color.occupied));
                            } else {
                                tv.setTextColor(getContext().getColor(R.color.free));
                            }
                        }
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return v;
    }

    private Date future(Date date){
        Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, 15);
        return calendar.getTime();
    }
}
