package com.example.ubfreeroom.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ubfreeroom.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class GalAdapter extends ArrayAdapter {

    private final HashMap<String, ArrayList<ArrayList<String>>> rooms;
    private final ArrayList<String> names;

    private int resc;

    public GalAdapter(Context context, int textViewResourceId, HashMap<String, ArrayList<ArrayList<String>>> objects, ArrayList<String> keys) {
        super(context, textViewResourceId, keys);
        resc = textViewResourceId;
        this.rooms = objects;
        this.names = keys;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v;
        @ColorInt int color = getContext().getColor(R.color.TBD);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(resc, null);
        TextView tv = v.findViewById(R.id.grid_tv);
        String name = names.get(position);
        String[] ls = name.split(" ");
        tv.setText(ls[ls.length - 1]);

        Date date = new Date();
        Date future = future(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
        short state = -100;

        ArrayList<ArrayList<String>> stamps = null;

        if (rooms.get(name) != null) {
            stamps = rooms.get(name);
            if (stamps.isEmpty()) {
                state = -99;
                color = getContext().getColor(R.color.free);
            } else if (stamps.get(0).get(0).matches("allDay")) {
                state = -98;
                color = getContext().getColor(R.color.occupied);
            } else {
                for (short i = 0; i < stamps.size(); ++i) {
                    ArrayList<String> dates = stamps.get(i);
                    try {
                        Date date1 = dateFormat.parse(dates.get(0).replace("T", " "));
                        if (future.before(date1)) {
                            state = i;
                            color = getContext().getColor(R.color.free);
                            break;
                        } else {
                            Date date2 = dateFormat.parse(dates.get(1).replace("T", " "));
                            if (date.before(date2)) {
                                state = i;
                                color = getContext().getColor(R.color.occupied);
                                break;
                            } else {
                                state = (short) (i+1);
                                color = getContext().getColor(R.color.free);
                            }
                        }
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }

        tv.setTextColor(color);

        short finalState = state;
        int finalColor = color;
        ArrayList<ArrayList<String>> finalStamps = stamps;

        tv.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_detailed);

            final TextView textView = dialog.findViewById(R.id.roomName);
            textView.setText(name);

            final Button button = dialog.findViewById(R.id.button);
            button.setOnClickListener(view1 -> dialog.dismiss());

            final LinearLayout layout1 = dialog.findViewById(R.id.infoLayout);

            final TextView textView0 = dialog.findViewById(R.id.tvState);
            final TextView textView1 = dialog.findViewById(R.id.infoStart);
            final TextView textView2 = dialog.findViewById(R.id.tvInfo);

            textView0.setTextColor(finalColor);

            if (finalColor == getContext().getColor(R.color.free)) {
                textView0.setText(R.string.free);
                textView1.setText(R.string.free_until);
                if (finalState == -99) {
                    textView2.setText(R.string.tomorrow);
                } else {
                    if(finalState == finalStamps.size()) {
                        textView2.setText(R.string.tomorrow);
                    } else {
                        ArrayList<String> dt = finalStamps.get(finalState);
                        textView2.setText(dt.get(0).replace("T", " "));
                    }

                }
            } else if (finalColor == getContext().getColor(R.color.occupied)) {
                textView0.setText(R.string.occupied);
                if (finalState == -98) {
                    textView1.setText(R.string.available);
                    textView2.setText(R.string.tomorrow);
                } else {
                    textView1.setText(R.string.available_at);
                    ArrayList<String> dt = finalStamps.get(finalState);
                    textView2.setText(dt.get(1).replace("T", " "));
                }
            } else {
                layout1.setVisibility(View.INVISIBLE);
            }

            dialog.show();
        });

        return v;
    }

    private Date future(Date date) {
        Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, 15);
        return calendar.getTime();
    }
}
