package com.example.ubfreeroom;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.ubfreeroom.databinding.ActivityMain2Binding;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Main2Activity extends AppCompatActivity {

    private final ConditionVariable condInit = new ConditionVariable();
    private final ConditionVariable condA9 = new ConditionVariable();
    private final ConditionVariable condA22 = new ConditionVariable();
    private final ConditionVariable condA28 = new ConditionVariable();
    private final ConditionVariable condA29 = new ConditionVariable();
    private final HashMap<String, ConditionVariable> variables = new HashMap<>();
    private final String TAG = "MainActivity";
    private ArrayList<String> ids = null;
    private AppBarConfiguration mAppBarConfiguration = null;
    private ActivityMain2Binding binding = null;
    private HTMLRequester requester = null;

    public ConditionVariable getCondInit() {
        return condInit;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public HTMLRequester getRequester() {
        return requester;
    }

    public void initRequester() {
        HashMap<String, InputStream> streams = new HashMap<>();
        streams.put("a29", getResources().openRawResource(R.raw.a29));
        streams.put("a28", getResources().openRawResource(R.raw.a28));
        streams.put("a22", getResources().openRawResource(R.raw.a22));
        streams.put("a9", getResources().openRawResource(R.raw.a9));

        try {
            requester = new HTMLRequester(streams);
            ids = requester.getUtilities().getIds();
        } catch (IOException e) {
            requester = null;
        }
    }

    public void queryEDT(String bat) {
        ArrayList<Object> resp = requester.requestBat(bat);
        if ((boolean) resp.get(0)) {
            HashMap<String, ArrayList<HashMap<String, Object>>> result = (HashMap<String, ArrayList<HashMap<String, Object>>>) resp.get(1);
            ArrayList<HashMap<String, Object>> list = result.get("result");

            Objects.requireNonNull(requester.getUtilities().getAllMaps().get(bat)).forEach((k, v) -> {
                ArrayList<ArrayList<String>> dates = new ArrayList<>();
                if (list != null && !list.isEmpty()) {
                    list.forEach(elem -> {
                        String desc = String.valueOf(Html.fromHtml(Objects.requireNonNull(elem.get("description")).toString(), Html.FROM_HTML_MODE_LEGACY));
                        if (desc.contains(k)) {
                            ArrayList<String> stamps = new ArrayList<>();
                            if (Objects.requireNonNull(elem.get("allDay")).toString().equals("true")) {
                                stamps.add("allDay");
                            } else {
                                stamps.add(Objects.requireNonNull(elem.get("start")).toString());
                                stamps.add(Objects.requireNonNull(elem.get("end")).toString());
                            }
                            dates.add(stamps);
                        }
                    });
                }
                Objects.requireNonNull(requester.getUtilities().getAllMaps().get(bat)).put(k, dates);
            });
            Objects.requireNonNull(variables.get(bat)).open();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        SharedPreferences preferences = getSharedPreferences("UBPref", Activity.MODE_PRIVATE);

        Runnable r = () -> {
            variables.put("a9", condA9);
            variables.put("a22", condA22);
            variables.put("a28", condA28);
            variables.put("a29", condA29);

            initRequester();
            if (requester != null) {
                ids.forEach(this::queryEDT);
            }
            condInit.open();
        };
        Thread thread = new Thread(r);
        thread.start();

        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;

        SwitchMaterial switchMaterial = navigationView.getMenu().getItem(3).getActionView().findViewById(R.id.app_switch);
        switchMaterial.setOnCheckedChangeListener((compoundButton, checked) -> {
            if (checked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            preferences.edit().putBoolean("Dark", checked).apply();
        });
        boolean isDarkModeOn = AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES || getSharedPreferences("UBPref", Activity.MODE_PRIVATE).getBoolean("Dark", false);
        switchMaterial.setChecked(isDarkModeOn);


        setSupportActionBar(binding.appBarMain2.toolbar);
        binding.appBarMain2.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        binding.appBarMain2.fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home, R.id.nav_gallery, R.id.nav_about).setOpenableLayout(drawer).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main2);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void recreate() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        startActivity(getIntent());
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main2);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }
}