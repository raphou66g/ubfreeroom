package com.example.ubfreeroom;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Utilities {

    private final ArrayList<String> ids = new ArrayList<>();
    private final HashMap<String, HashMap<String, ArrayList<ArrayList<String>>>> allMaps;
    private HashMap<String, ArrayList<ArrayList<String>>> a9map;
    private HashMap<String, ArrayList<ArrayList<String>>> a22map;
    private HashMap<String, ArrayList<ArrayList<String>>> a28map;
    private HashMap<String, ArrayList<ArrayList<String>>> a29map;

    public Utilities(Map<String, InputStream> streams) {
        String[] list = {"a9", "a22", "a28", "a29"};
        Collections.addAll(ids, list);
        allMaps = new HashMap<>();
        a9map = new HashMap<>();
        a22map = new HashMap<>();
        a28map = new HashMap<>();
        a29map = new HashMap<>();
        for (String s : list) {
            try (InputStream is = streams.get(s); BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    switch (s) {
                        case "a9" -> a9map.put(line, null);
                        case "a22" -> a22map.put(line, null);
                        case "a28" -> a28map.put(line, null);
                        case "a29" -> a29map.put(line, null);
                    }
                }
            } catch (Exception e) {
                System.out.println("Err " + e);
            }
            switch (s) {
                case "a9" -> allMaps.put(s, a9map);
                case "a22" -> allMaps.put(s, a22map);
                case "a28" -> allMaps.put(s, a28map);
                case "a29" -> allMaps.put(s, a29map);
            }
        }
    }

    public HashMap<String, HashMap<String, ArrayList<ArrayList<String>>>> getAllMaps() {
        return allMaps;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getA29map() {
        return a29map;
    }

    public void setA29map(HashMap<String, ArrayList<ArrayList<String>>> a29map) {
        this.a29map = a29map;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getA28map() {
        return a28map;
    }

    public void setA28map(HashMap<String, ArrayList<ArrayList<String>>> a28map) {
        this.a28map = a28map;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getA22map() {
        return a22map;
    }

    public void setA22map(HashMap<String, ArrayList<ArrayList<String>>> a22map) {
        this.a22map = a22map;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getA9map() {
        return a9map;
    }

    public void setA9map(HashMap<String, ArrayList<ArrayList<String>>> a9map) {
        this.a9map = a9map;
    }
}
