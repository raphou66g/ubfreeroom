package com.example.ubfreeroom.ui.gallery;

import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ubfreeroom.HTMLRequester;
import com.example.ubfreeroom.Main2Activity;
import com.example.ubfreeroom.R;
import com.example.ubfreeroom.adapters.GalAdapter;
import com.example.ubfreeroom.databinding.FragmentGalleryBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class GalleryFragment extends Fragment {

    private final String TAG = "GalFrag";

    private ConditionVariable condDrop = new ConditionVariable();
    private FragmentGalleryBinding binding;
    private HTMLRequester requester = null;

    private Spinner spinner = null;
    private GridView grid = null;
    private Main2Activity activity = null;

    private HashMap<String, ArrayList<ArrayList<String>>> goodMap;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentGalleryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        activity = ((Main2Activity) requireActivity());
        grid = binding.grid;
        spinner = binding.spinner;
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateGrid();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        Runnable runnable = () -> {
            activity.getCondInit().block();
            requester = activity.getRequester();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, activity.getIds());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            condDrop.open();
        };
        Handler handler = new Handler();
        handler.postDelayed(runnable, 1000);


        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void getGoodMap() {
        String s = (String) spinner.getSelectedItem();
        goodMap = requester.getUtilities().getAllMaps().get(s);
    }

    private void updateGrid() {
        condDrop.block();
        getGoodMap();
        ArrayList<String> names = new ArrayList<>(goodMap.keySet());
        Collections.sort(names);
        grid.setAdapter(new GalAdapter(this.getContext(), R.layout.gallery_items, goodMap, names));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}