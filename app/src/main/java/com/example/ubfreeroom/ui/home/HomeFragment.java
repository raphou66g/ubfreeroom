package com.example.ubfreeroom.ui.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ubfreeroom.HTMLRequester;
import com.example.ubfreeroom.Main2Activity;
import com.example.ubfreeroom.R;
import com.example.ubfreeroom.adapters.GVAdapter;
import com.example.ubfreeroom.databinding.FragmentHomeBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

public class HomeFragment extends Fragment {

    private final String TAG = "Fragment";
    private HTMLRequester requester = null;
    private FragmentHomeBinding binding;
    private Main2Activity activity = null;
    private HashMap<String, GridView> gridViewsMap = new HashMap<>();
    private HashMap<String, GVAdapter> gvAdapterMap = new HashMap<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        activity = ((Main2Activity) requireActivity());

        ImageButton button = binding.imageButton;
        button.setOnClickListener(this::updateGrid);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        Runnable runnable = this::fillGrid;

        Handler handler = new Handler();
        handler.postDelayed(runnable, 2000);
    }

    private void fillGrid() {
        activity.getCondInit().block();
        buildAdapters();
        activity.getIds().forEach(s -> (Objects.requireNonNull(gridViewsMap.get(s))).setAdapter((gvAdapterMap.get(s))));
        binding.progressBar.setVisibility(View.INVISIBLE);
        binding.progressBar2.setVisibility(View.INVISIBLE);
        binding.progressBar3.setVisibility(View.INVISIBLE);
        binding.progressBar4.setVisibility(View.INVISIBLE);
    }


    private void updateGrid(View view) {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.progressBar2.setVisibility(View.VISIBLE);
        binding.progressBar3.setVisibility(View.VISIBLE);
        binding.progressBar4.setVisibility(View.VISIBLE);

        Runnable runnable = this::fillGrid;
        Handler handler = new Handler();
        handler.postDelayed(runnable, 2000);

    }

    private GVAdapter adapterA9() {
        ArrayList<String> names = new ArrayList<>(requester.getUtilities().getA9map().keySet());
        Collections.sort(names);
        return new GVAdapter(this.getContext(), R.layout.grid_view_items, requester.getUtilities().getA9map(), names);
    }

    private GVAdapter adapterA22() {
        ArrayList<String> names = new ArrayList<>(requester.getUtilities().getA22map().keySet());
        Collections.sort(names);
        return new GVAdapter(this.getContext(), R.layout.grid_view_items, requester.getUtilities().getA22map(), names);
    }

    private GVAdapter adapterA28() {
        ArrayList<String> names = new ArrayList<>(requester.getUtilities().getA28map().keySet());
        Collections.sort(names);
        return new GVAdapter(this.getContext(), R.layout.grid_view_items, requester.getUtilities().getA28map(), names);
    }

    private GVAdapter adapterA29() {
        ArrayList<String> names = new ArrayList<>(requester.getUtilities().getA29map().keySet());
        Collections.sort(names);
        return new GVAdapter(this.getContext(), R.layout.grid_view_items, requester.getUtilities().getA29map(), names);
    }

    private void buildAdapters() {
        requester = activity.getRequester();
        gridViewsMap = new HashMap<>();
        gvAdapterMap = new HashMap<>();
        CountDownLatch latch = new CountDownLatch(4);
        activity.getIds().forEach(s -> {
            switch (s) {
                case "a9" -> gridViewsMap.put(s, binding.grid1);
                case "a22" -> gridViewsMap.put(s, binding.grid2);
                case "a28" -> gridViewsMap.put(s, binding.grid3);
                case "a29" -> gridViewsMap.put(s, binding.grid4);
            }

            Runnable runnable = () -> {
                switch (s) {
                    case "a9" -> gvAdapterMap.put(s, adapterA9());
                    case "a22" -> gvAdapterMap.put(s, adapterA22());
                    case "a28" -> gvAdapterMap.put(s, adapterA28());
                    case "a29" -> gvAdapterMap.put(s, adapterA29());
                }
                latch.countDown();
            };

            Thread thread = new Thread(runnable);
            thread.start();
        });
        try {
            latch.await();
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        activity = null;
    }
}