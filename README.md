# UBFreeRoom

## Description

UBFreeRoom est une application Android qui fonctionne sur la base de l'emplois du temps en ligne de l'Université de Bordeaux - Talence, Collège ST.

Par le biais d'un jeu de requête http, l'application récupère l'emplois du temps salle par salle et compile les résultats dans le but de nous indiquer quelles salles sont libres ou non, à partir ou jusqu'à quelles heures elles le sont.

## Pourquoi du comment ?

J'ai eu l'occasion de chercher par moi même une salle libre à la fac pour de nombreuses raisons, mais la recherche était fastidieuse. J'ai donc voulu automatisé le processus.

En regardant l'emplois du temps en ligne, et en farfouillant dans le DevTool de mon navigateur à la recherche d'une solution, je me suis aperçu que je pouvais voir toutes les requêtes émisent dans la rubrique Network.

C'est à partir de ce moment que j'ai décidé d'exécuter ces requêtes de façon groupé via une application Android.
